class EasyWebHooksController < ApplicationController

  before_filter :find_optional_project
  before_filter :find_easy_web_hooks, :only => [:index]
  before_filter :find_easy_web_hook, :only => [:edit, :update, :destroy]
  before_filter :authorize
  before_filter :build_new_easy_web_hook, :only => [:new, :create]

  def index
  end

  def new
  end

  def create
    if @easy_web_hook.save
      flash[:notice] = l(:notice_successful_create)

      if params[:continue]
        redirect_to new_easy_web_hook_path(@project)
      else
        redirect_to easy_web_hooks_path(@project)
      end
    else
      render :action => 'new'
    end
  end

  def edit
  end

  def update
    if params[:easy_web_hook].present?
      @easy_web_hook.safe_attributes = params[:easy_web_hook]

      if @easy_web_hook.save
        redirect_to easy_web_hooks_path(@project), :notice => l(:notice_successful_update)
      else
        render :action => 'edit'
      end
    end
  end

  def destroy
    @easy_web_hook.destroy
    redirect_to easy_web_hooks_path(@project), :notice => l(:notice_successful_delete)
  end

  private

  def find_easy_web_hooks
    if @project
      @easy_web_hooks = @project.easy_web_hooks
    else
      @easy_web_hooks = EasyWebHook.where(:project_id => nil).order(:name) || []
      @easy_web_hooks.concat(Project.allowed_to(:view_easy_web_hooks).visible.sorted.
                                     preload(:easy_web_hooks).collect(&:easy_web_hooks).flatten)
    end
  end

  def find_easy_web_hook
    @easy_web_hook = EasyWebHook.find(params[:id]) if params[:id].present?
  rescue ActiveRecord::RecortNotFound
    return render_404
  end

  def build_new_easy_web_hook
    @easy_web_hook = EasyWebHook.new
    @easy_web_hook.safe_attributes = params[:easy_web_hook] if params[:easy_web_hook].present?
    @easy_web_hook.project = @project if @project
    @easy_web_hook.author = User.current
  end

  def authorize
    return render_403 if params[:action].in?(["edit", "update", "destroy"]) && !@easy_web_hook.editable?

    super(params[:controller], params[:action], !@project)
  end

end
