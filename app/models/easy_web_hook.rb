class EasyWebHook < ActiveRecord::Base
  include Redmine::SafeAttributes

  ENTITY_TYPES = %w(Issue Project TimeEntry User)
  ENTITY_ACTIONS = %w(create update destroy)
  ENTITY_DATA_TYPES = %w(json xml)

  belongs_to :project
  belongs_to :author, :class_name => 'User'

  enum :status => { :active => 1, :inactive => 0 }

  validates :name, :entity_type, :action, :url, :author, :presence => true
  validates :action, :inclusion => { :in => ENTITY_ACTIONS }
  validates :entity_data_type, :inclusion => { :in => ENTITY_DATA_TYPES }
  validates :url, :format => { :with => URI.regexp }, :if => Proc.new {|hook| hook.url.present? }
  validate :validate_author_with_permission

  before_save :ensure_attributes

  safe_attributes 'name', 'entity_type', 'action', 'url', 'entity_data_type',
                  'project_id', 'author_id', 'status', :if => proc {|x| x.editable? }

  attr_protected :id

  def editable?(user = nil)
    user ||= User.current
    if project
      user.allowed_to?(:edit_easy_web_hooks, project)
    else
      user.allowed_to_globally?(:edit_easy_web_hooks)
    end
  end

  private

  def ensure_attributes
    project = nil if entity_type.constantize.is_a?(Principal)
    author ||= User.current
  end

  def validate_author_with_permission
    unless editable?
      if project
        errors.add(:base, l(:error_no_permission_for_this_project, :project => project.to_s))
      else
        errors.add(:base, l(:error_no_permission))
      end
    end
  end

end
