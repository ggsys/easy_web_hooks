module EasyWebHooksHelper

  def entity_type_options_for_select
    EasyWebHook::ENTITY_TYPES.collect do |entity_type|
      entity_label = entity_type == "TimeEntry" ? l(:label_spent_time) : l("label_#{entity_type.downcase}")
      [entity_label, entity_type]
    end
  end

end
