class CreateEasyWebHooks < ActiveRecord::Migration
  def change
    create_table :easy_web_hooks, force: true do |t|
      t.string :name
      t.string :entity_type, index: true, null: false
      t.string :action, null: false
      t.string :url, null: false
      t.string :entity_data_type, null: false, default: 'json'
      t.references :project
      t.references :author, null: false
      t.integer :status, default: 0, index: true

      t.timestamps null: false

      t.index([:status, :action], name: 'index_status_action')
      t.index([:status, :action, :project_id], name: 'index_status_action_project_id')

      t.index([:entity_type, :status, :action], name: 'index_entity_type_status_action')
      t.index([:entity_type, :status, :action, :project_id], name: 'index_entity_type_status_action_project_id')
    end
  end
end
\
