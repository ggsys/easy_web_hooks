Dir[File.dirname(__FILE__) + '/lib/easy_web_hooks/redmine_patch/*/*.rb'].each { |file| require_dependency file }

ActionDispatch::Reloader.to_prepare do
  require 'easy_web_hooks/hook_request'
end

ActiveSupport.on_load(:easyproject, yield: true) do

  # Redmine::MenuManager.map :project_menu do |menu|
  #   menu.push :easy_web_hooks,
  #             {controller: 'easy_web_hooks', action: 'index'},
  #             param: :project_id,
  #             if: proc { |project| project.module_enabled?(:easy_web_hooks) && User.current.allowed_to?(:view_easy_web_hooks, project) }
  # end
  #
  # Redmine::MenuManager.map :top_menu do |menu|
  #   menu_options = {if: proc { User.current.allowed_to_globally?(:view_easy_web_hooks) }}
  #   menu_options[:after] = (Redmine::Plugin.installed?(:easy_gantt) && :easy_gantt || :documents)
  #   menu_options[:parent] = :others if defined?(EasyExtensions)
  #
  #   menu.push :easy_web_hooks, :easy_web_hooks_path, menu_options
  # end

  Redmine::AccessControl.map do |map|
    map.project_module :easy_web_hooks do |pmap|
      pmap.permission :view_easy_web_hooks, {easy_web_hooks: [:index, :show]}
      pmap.permission :edit_easy_web_hooks, {easy_web_hooks: [:new, :create, :edit, :update, :destroy]}
    end
  end
end
