scope '(projects/:project_id)' do
  resources :easy_web_hooks, :except => [:show]
end
