Redmine::Plugin.register :easy_web_hooks do
  name 'Easy Web Hooks plugin'
  author 'Easy Software Ltd'
  author_url 'www.easysoftware.cz'
  description 'Create a hook to send a notification about entity action on specified URL'
  version '2016'

  if Redmine::Plugin.installed?(:easy_extensions)
    disabled Rails.env.test?
  end
end

if Redmine::Plugin.registered_plugins[:easy_extensions].nil?
  require_relative 'after_init'
end
