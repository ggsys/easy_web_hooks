module EasyWebHooks
  module ProjectPatch

    def self.included(base)
      base.send(:include, EasyWebHooks::HookRequest)
      base.class_eval do

        has_many :easy_web_hooks, -> { order(:name) }

        def sql_for_easy_web_hook_project_id
          project_ids = ancestors.pluck(:id)
          where_clause = project_ids.present? ? "project_id IN (#{project_ids.join(', ')}) OR " : ""
          where_clause << "project_id IS NULL"
        end

        def easy_web_hook_entity_params
          %w(id name parent_id)
        end

      end

    end

  end
end
RedmineExtensions::PatchManager.register_model_patch 'Project', 'EasyWebHooks::ProjectPatch'
