module EasyWebHooks
  module TimeEntryPatch

    def self.included(base)
      base.send(:include, EasyWebHooks::HookRequest)
      base.class_eval do

        def easy_web_hook_entity_params
          %w(id project_id user_id issue_id hours comments activity)
        end

      end

    end

  end
end
RedmineExtensions::PatchManager.register_model_patch 'TimeEntry', 'EasyWebHooks::TimeEntryPatch'
