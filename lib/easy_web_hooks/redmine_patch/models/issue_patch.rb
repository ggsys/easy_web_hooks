module EasyWebHooks
  module IssuePatch

    def self.included(base)
      base.send(:include, EasyWebHooks::HookRequest)
      base.class_eval do

        def easy_web_hook_entity_params
          %w(id subject project_id author_id assigned_to_id)
        end

      end

    end

  end
end
RedmineExtensions::PatchManager.register_model_patch 'Issue', 'EasyWebHooks::IssuePatch'
