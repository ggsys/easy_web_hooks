module EasyWebHooks
  module UserPatch

    # TODO
    # fix EasyWebHook: user web hooks have no project! (now required in UI)
    def self.included(base)
      base.send(:include, EasyWebHooks::HookRequest)
      base.class_eval do


        def can_edit_hooks?(project = nil)
          if project
            User.current.allowed_to?(:edit_easy_web_hooks, project)
          else
            User.current.allowed_to_globally?(:edit_easy_web_hooks)
          end
        end

        def sql_for_easy_web_hook_project_id
          'project_id IS NULL'
        end

        def easy_web_hook_entity_params
          %w(id firstname lastname)
        end

      end

    end

  end
end
RedmineExtensions::PatchManager.register_model_patch 'User', 'EasyWebHooks::UserPatch'
