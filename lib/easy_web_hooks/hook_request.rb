module EasyWebHooks
  module HookRequest
    def self.included(base)
      base.class_eval do

        after_create :easy_web_hook_after_create
        after_update :easy_web_hook_after_update
        after_destroy :easy_web_hook_after_destroy

        private

        # todo?
        # allow POST request in hook administration? - due to request length limit
        # allow change attribute name (entity_type=) in hook administration? - token gsub?
        def send_request(action)
          EasyWebHook.active.where(:action => action).where(sql_for_easy_web_hook_project_id).each do |hook|
            uri = URI.parse(hook.url)
            http = Net::HTTP.new(uri.host, uri.port)

            uri.query = [uri.query, format_entity_to_uri_param(hook)].compact.join('&')

            request = Net::HTTP::Get.new(uri.request_uri)
            response = http.request(request)
          end
          true
        end

        def should_send_request?(action)
          EasyWebHook.table_exists? && EasyWebHook.active.where(:action => action).where(sql_for_easy_web_hook_project_id).exists?
        end

        def easy_web_hook_after_create
          easy_delay.send_request(:create) if should_send_request?(:create)
        end

        def easy_web_hook_after_update
          easy_delay.send_request(:update) if should_send_request?(:update)
        end

        def easy_web_hook_after_destroy
          easy_delay.send_request(:destroy) if should_send_request?(:destroy)
        end

        def easy_web_hook_entity_params
          self.attribute_names
        end

        def format_entity_to_uri_param(easy_web_hook)
          entity_formatted = case easy_web_hook.entity_data_type
                               when 'json'
                                 to_json(:only => easy_web_hook_entity_params)
                               when 'xml'
                                 to_xml(:only => easy_web_hook_entity_params)
                             end
          URI.encode("#{self.class.name.underscore}=#{entity_formatted}")
        end

        def sql_for_easy_web_hook_project_id
          return nil unless respond_to?(:project_id)
          {:project_id => [project_id, nil]}
        end

      end
    end
  end
end
